﻿using UnityEngine;

namespace KS.AI.FSM.Samples.ZombieAI
{
    public class Attack : State
    {
        public ZombieAIFSM localFSM { get; set; }
        
        public Attack(FiniteStateMachine fsm) : base(fsm)
        {
            localFSM = (ZombieAIFSM)fsm;

            //TODO: delete this line
            localFSM.CurrentStateName = this.GetType().Name;
        }

        public override void Enter()
        {
            localFSM.Anim.SetTrigger("Attack");

            localFSM.StopAINavigation();

            base.Enter();
        }

        public override void Update()
        {
            if (!localFSM.IsPlayerInAttackRange)
            {
                if (localFSM.IsPlayerBehind())
                {
                    FSM.NextState = new Idle(FSM);
                    this.StateStage = StateEvent.EXIT;
                }
                else
                {
                    FSM.NextState = new RunChasing(FSM);
                    this.StateStage = StateEvent.EXIT;
                }
            }
        }
        
        public override void Exit()
        {
            localFSM.Anim.ResetTrigger("Attack");

            localFSM.StopAINavigation();


            base.Exit();
        }
    }
}
