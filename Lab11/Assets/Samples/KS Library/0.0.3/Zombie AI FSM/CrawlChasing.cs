﻿using UnityEngine;


namespace KS.AI.FSM.Samples.ZombieAI
{
    public class CrawlChasing : State
    {
        public ZombieAIFSM localFSM { get; set; }
        
        public CrawlChasing(FiniteStateMachine fsm) : base(fsm)
        {
            localFSM = (ZombieAIFSM)fsm;

            //TODO: delete this line
            localFSM.CurrentStateName = this.GetType().Name;
            
            localFSM.Agent.speed = 2;

        }
        
        public override void Enter()
        {
            localFSM.AIChar.SetTarget(localFSM.player);
            localFSM.Agent.SetDestination(localFSM.player.position);


            localFSM.Anim.SetTrigger("Crawl");
            
            //Proceed to the next stage of the FSM's state
            base.Enter();
        }
        public override void Update()
        {
            #region Chasing the player

            localFSM.Agent.SetDestination(localFSM.player.position);
            
            if (localFSM.Agent.remainingDistance > localFSM.Agent.stoppingDistance)
            {
                //Move the agent
                localFSM.ThirdPersonChar.Move(localFSM.Agent.desiredVelocity, false, false);
            }

            #endregion
            
            #region Checking if the player is escaped successfully/behind

            if (localFSM.IsPlayerBehind())
            {
                if (Random.Range(0, 100) > 50)
                {
                    FSM.NextState = new Idle(FSM);
                    this.StateStage = StateEvent.EXIT;
                }
                else
                {
                    FSM.NextState = new Scream(FSM);
                    this.StateStage = StateEvent.EXIT;
                }
            }

            #endregion
            
            
            if (localFSM.IsPlayerInAttackRange)
            {
                FSM.NextState = new Attack(FSM);
                this.StateStage = StateEvent.EXIT;
            }
        }
        
        public override void Exit()
        {

            localFSM.Anim.ResetTrigger("Crawl");

            localFSM.StopAINavigation();

            base.Exit();
        }
    }
}
