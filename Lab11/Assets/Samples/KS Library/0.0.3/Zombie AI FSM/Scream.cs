﻿using UnityEngine;

namespace KS.AI.FSM.Samples.ZombieAI
{
    public class Scream : State
    {
        public ZombieAIFSM localFSM { get; set; }
        
        private float delayTime;
        
        public Scream(FiniteStateMachine fsm) : base(fsm)
        {
            localFSM = (ZombieAIFSM)fsm;

            //TODO: delete this line
            localFSM.CurrentStateName = this.GetType().Name;
        }

        public override void Enter()
        {
            localFSM.Anim.SetTrigger("Scream");
            delayTime = Random.Range(2, 5);
            base.Enter();
        }

        public override void Update()
        {
            delayTime -= Time.deltaTime;
            if (delayTime <= 0)
            {
                //Prepare to go to the next state, Patrol
                localFSM.NextState = new Idle(localFSM);
                this.StateStage = StateEvent.EXIT;
            }
        }

        public override void Exit()
        {
            
            localFSM.Anim.ResetTrigger("Scream");
            base.Exit();
        }
        
    }
}
